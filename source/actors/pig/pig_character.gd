extends Position2D

enum directions{LEFT, RIGHT}
enum faces{NORMAL, MAD, STUN, CONCERN, HAPPY, THINK, WINK}

var face_rect = Rect2(0, 52, 70, 74)
var faces_list = []
export (float) var speed = 200
onready var init_scale = get_scale()

func _ready():
	for i in range(0, faces.size()):
		faces_list.append(face_rect)
	faces_list[1].position += Vector2(128, 0)
	faces_list[2].position += Vector2(0, 128)
	faces_list[3].position += Vector2(128, 128)
	faces_list[4].position += Vector2(0, 128 * 2)
	faces_list[5].position += Vector2(128, 128 * 2)
	faces_list[6].position += Vector2(0, 128 * 3)

func flip(side):
	if side == LEFT:
		scale.x = -init_scale.x
	else:
		scale.x = init_scale.x

func _on_animation_finished(anim_name):
	pass

func _on_animation_started(anim_name):
	if "stun" in anim_name:
		set_face(STUN)
	elif "concern" in anim_name:
		set_face(CONCERN)
	elif "kidding" in anim_name or "happy" in anim_name:
		set_face(HAPPY)
	elif "think" in anim_name:
		set_face(THINK)
	elif "wink" in anim_name:
		set_face(WINK)
	else:
		set_face(NORMAL)
		
	if "front" in anim_name:
		$sprites/body.hide()
		$sprites/body_front.show()
	else:
		$sprites/body_front.hide()
		$sprites/body.show()
		
	if "thumb" in anim_name:
		$sprites/body/arm_l.region_rect = Rect2(128, 226, 31, 30)
		$sprites/body_front/arm_l.region_rect = Rect2(128, 226, 31, 30)
		$sprites/body/arm_l.z_index = 4
		$sprites/body_front/arm_l.z_index = 4
	elif "think" in anim_name:
		$sprites/body/arm_l.region_rect = Rect2(192, 222, 34, 35)
		$sprites/body_front/arm_l.region_rect = Rect2(192, 222, 34, 35)
		$sprites/body/arm_l.z_index = 4
		$sprites/body_front/arm_l.z_index = 4
	else:
		$sprites/body/arm_l.region_rect = Rect2(192, 166, 26, 26)
		$sprites/body_front/arm_l.region_rect = Rect2(192, 166, 26, 26)
		$sprites/body/arm_l.z_index = -1
		$sprites/body_front/arm_l.z_index = -1
		
		
func set_face(expression):
	$sprites/body/head.region_rect = faces_list[expression]
	$sprites/body_front/head.region_rect = faces_list[expression]
	$sprites/body_front/head.region_rect.position += Vector2(128*2, 0)