extends TextureButton

onready var tooltip = $tooltip
func pop_in():
	$tween.interpolate_property(self, "rect_scale", Vector2(0 ,0), 
		Vector2(1, 1), 0.25, Tween.TRANS_BACK, Tween.EASE_OUT)
	$tween.start()
	yield($tween, "tween_completed")

func pop_out():
	$tween.interpolate_property(self, "rect_scale", Vector2(1, 1), 
		Vector2(0 ,0), 0.25, Tween.TRANS_BACK, Tween.EASE_IN)
	$tween.start()
	yield($tween, "tween_completed")
	queue_free()

func _on_ingredient_mouse_entered():
	self_modulate = Color(1.3, 1.3, 1.3, 1.0)
	$timer.start()

func _on_ingredient_mouse_exited():
	self_modulate = Color(1.0, 1.0, 1.0, 1.0)
	$timer.stop()
	tooltip.hide()

func _on_ingredient_button_down():
	self_modulate = Color(0.7, 0.7, 0.7, 1.0)
	$timer.stop()
	tooltip.hide()
	
func _on_ingredient_button_up():
	self_modulate = Color(1.3, 1.3, 1.3, 1.0)
	$timer.stop()
	tooltip.hide()

func _on_timer_timeout():
	var transparent = Color(1.0, 1.0, 1.0, 0.0)
	var opaque = Color(1.0, 1.0, 1.0, 1.0)
	tooltip.self_modulate = transparent
	tooltip.show()
	$tween.interpolate_property(tooltip, "self_modulate", transparent, opaque,
		0.125, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.start()
